##Safetydome Portal

In the Safetydome, two men enter, and both eventually leave. It is a friendly fighting arena where the combantants compete for glory and cuddles.

There exists a database schema modeling the sundry combatants and their skills that you are already familiar with. Now that the fights are over, it is time to extract that information and present it in a pretty web page.

##URL Schema

Index Page – /
  Contains links to the Combatant List, Battle List, and the Ranking Page.
Combatant List – /combatant
  Display a alphabetical, clickable list of combatants. Clicking on a combatant should take the user to that combatant's detail page.
Combatant Detail Page – /combatant/<id>
  Display information about the given combatant.
Battle List – /battle
  Display a clickable list of battles. Any order is acceptable as long as it is consistent between page loads. Clicking on a battle should take the user to that battle's detail page.
Battle Detail Page – /battle/<id>-<id>
  Display information about the given battle between the combatants with the two given id values.
Ranking Page – /results
  Display a list of combatants, ordered by the number of wins (most wins first). Clicking on a combatant should take the user to that combatant's detail page.

##Requirements

  * Any words must agree with their number: "1 Win" vs. "1 Wins".
  * The project should be in a github project named safetydome.
  * The application should connect to a database called safetydome, with username/password as flask/flask. This user role will not have write access to the data, only read access.
  * The application should listen on port (7000 + uid), for your given UID on the devprep machine (echo $UID).
  *SQL injection potential will be treated very harshly. Yes, regardless of the locked-down nature of the user role.

##Considerations

  * Does the database exist? Can it be connected to?
  * How will the application respond if an unknown page is requested? If an unknown combatant or battle is asked for? If there are no battles recorded?

##Flourishes

When it comes to HTML, the sky is the limit. Prettify the web pages. Add Javascript, add CSS, add graphics. Add additional pages. Ensure W3C HTML compliance. Ensure WCAG compliance.
