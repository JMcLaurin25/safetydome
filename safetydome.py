#!/usr/bin/python3

from flask import Flask, abort, redirect, url_for, render_template
import psycopg2
import sys

app = Flask(__name__)

conn = psycopg2.connect(
        database='safetydome',
        user='flask',
        password='flask',
        host='localhost'
        )
cur = conn.cursor()


class Battle():
    """Battle object used to store battle combatants and statistics"""
    __tablename__ = 'Battle'

    def __init__(
            self,
            id,
            one_id,
            two_id,
            start=None,
            stop=None,
            one_name=None,
            two_name=None
            ):
        self.id = id
        self.one_id = one_id
        self.two_id = two_id
        self.one_name = one_name
        self.two_name = two_name
        self.start = start
        self.stop = stop


class Combatant():
    """Combatant class stores fighter attributes"""
    __tablename__ = 'combatant'

    def __init__(
            self,
            comb_id,
            comb_name,
            comb_species,
            atk_name=None,
            atk_type=None,
            atk_mn_dmg=None,
            atk_mx_dmg=None,
            atk_spd=None
            ):
        self.id = comb_id
        self.name = comb_name
        self.species = comb_species
        self.attack = []
        self.attack.append(atk_name)
        self.attack_type = []
        self.attack_type.append(atk_type)
        self.min_dmg = []
        self.min_dmg.append(atk_mn_dmg)
        self.max_dmg = []
        self.max_dmg.append(atk_mx_dmg)
        self.speed = []
        self.speed.append(atk_spd)


@app.route('/')
def index_proc():
    """Default index.html function. Renders initial page"""
    return render_template('index.html')


@app.route('/combatant')
def combatant_proc():
    """Function that processes the list of combatants"""
    query_combatant = "SELECT combatant.id, combatant.name, species.name "
    query_combatant += "FROM public.combatant, public.species "
    query_combatant += "WHERE combatant.species_id = species.id "
    query_combatant += "ORDER by combatant.name"

    comb_objs = []

    # Connection to retrieve combatants
    try:
        cur.execute(query_combatant)
        data_combatants = cur.fetchall()
    except Exception as e:
        failure = "\n  -Failed to query combatant data. {0}"
        print(failure.format(e), file=sys.stderr)
        abort(404)

    for entry in data_combatants:
        current = Combatant(entry[0], entry[1], entry[2])
        comb_objs.append(current)

    return render_template('combatants.html', combatants=comb_objs)


@app.route('/combatant/<id>')
def fighter_proc(id=None):
    """Processes the request for a specific combatant id"""
    if (str(id).isnumeric() is not True):
        abort(404)
    query_fighter = "SELECT combatant.id, combatant.name, species.name, "
    query_fighter += "attack.name, attack.type, attack.min_dmg, "
    query_fighter += "attack.max_dmg, attack.speed "
    query_fighter += "FROM public.combatant, public.species, "
    query_fighter += "public.species_attack, public.attack "
    query_fighter += "WHERE combatant.species_id = species.id AND "
    query_fighter += "species.id = species_attack.species_id AND "
    query_fighter += "species_attack.attack_id = attack.id AND combatant.id = "
    query_fighter += str(id)

    # Connection to retrieve fighter data
    try:
        cur.execute(query_fighter)
        data_fighter = cur.fetchall()
    except Exception as e:
        failure = "\n  -Failed to query fighter data. {0}"
        print(failure.format(e), file=sys.stderr)
        abort(404)

    # Create initial Combatant Object
    fighter = Combatant(
            data_fighter[0][0],
            data_fighter[0][1],
            data_fighter[0][2]
            )
    for data in data_fighter:
        fighter.attack.append(data[3])
        fighter.attack_type.append(data[4])
        fighter.min_dmg.append(data[5])
        fighter.max_dmg.append(data[6])
        fighter.speed.append(data[7])

    return render_template('fighter.html', fighter=fighter)


@app.route('/battle')
@app.route('/battle/<id>')
@app.route('/battle/<id>-<id2>')
def battle_proc(id=None, id2=None):
    """Function handles all battle html page calls"""
    # Single ID passed to /battle
    if (id is not None and id2 is None):
        if (str(id).isnumeric() is not True):
            abort(404)
        query_fights = "SELECT fight.id, fight.combatant_one, "
        query_fights += "fight.combatant_two, fight.winner, fight.start, "
        query_fights += "fight.finish FROM public.fight WHERE fight.id = "
        query_fights += str(id)

    # Two ID's passed to /battle
    elif (id is not None and id2 is not None):
        if (str(id).isnumeric() is not True):
            if (str(id2).isnumeric is not True):
                abort(404)
        query_fights = "SELECT fight.id, fight.combatant_one, "
        query_fights += "fight.combatant_two, fight.winner, fight.start, "
        query_fights += "fight.finish FROM public.fight WHERE combatant_one = "
        query_fights += str(id)
        query_fights += " AND combatant_two = "
        query_fights += str(id2)
        query_fights += " OR combatant_one = "
        query_fights += str(id2)
        query_fights += " AND combatant_two = "
        query_fights += str(id)

    # No ID's passed to /battle
    else:
        query_fights = "SELECT fight.id, fight.combatant_one, "
        query_fights += "fight.combatant_two, fight.winner, fight.start, "
        query_fights += "fight.finish FROM public.fight"

    # Connection to retrieve fight data
    try:
        cur.execute(query_fights)
        data_fight = cur.fetchall()
    except Exception as e:
        failure = "\n  -Failed to query fight data. {0}"
        print(failure.format(e), file=sys.stderr)
        abort(404)

    # Create array of battles
    fights = []
    for data in data_fight:
        new = Battle(data[0], data[1], data[2], data[4], data[5])

        # Retrieve Fighter one
        query_one = "SELECT combatant.name FROM public.combatant WHERE "
        query_one += "combatant.id = "
        query_one += str(new.one_id)

        try:
            cur.execute(query_one)
            name_one = cur.fetchall()
        except:
            print("\n  -Failed to query name. {0}".format(e), file=sys.stderr)

        new.one_name = name_one[0][0]

        # Retrieve Fighter two
        query_two = "SELECT combatant.name FROM public.combatant WHERE "
        query_two += "combatant.id = "
        query_two += str(new.two_id)

        try:
            cur.execute(query_two)
            name_two = cur.fetchall()
        except:
            print("\n  -Failed to query name. {0}".format(e), file=sys.stderr)

        new.two_name = name_two[0][0]

        if data[3] == 'One':
            new.winner = new.one_name
        elif data[3] == 'Two':
            new.winner = new.two_name
        else:
            new.winner = 'Tie'
        fights.append(new)

    if id is not None:
        return render_template('battle_data.html', battle=fights[0])
    else:
        return render_template('battle.html', fights=fights)


@app.route('/results')
def results_proc():
    """Queries the database for win results"""
    query_win = "SELECT id, count(*) as wins FROM ("
    query_win += "SELECT CASE "
    query_win += "WHEN winner = 'One' THEN combatant_one "
    query_win += "WHEN winner = 'Two' THEN combatant_two "
    query_win += "end AS id, COUNT(*) AS wins FROM fight "
    query_win += "GROUP BY id "
    query_win += "ORDER BY wins desc) AS wins WHERE ID IS NOT NULL GROUP BY "
    query_win += "id ORDER BY wins DESC"

    # Connection to retrieve fighter data
    try:
        cur.execute(query_win)
        data_win = cur.fetchall()
    except Exception as e:
        failure = "\n  -Failed to query fighter data. {0}"
        print(failure.format(e), file=sys.stderr)
        abort(404)

    # Number top ranked and create list of queried results
    rank = 1
    combatants = []
    for win in data_win:
        # Retrieve Fighter one
        query_name = "SELECT combatant.name, species.name FROM "
        query_name += "public.combatant, public.species WHERE "
        query_name += "combatant.species_id = species.id and combatant.id = "
        query_name += str(win[0])

        try:
            cur.execute(query_name)
            name = cur.fetchall()
        except Exception as e:
            failure = "\n  -Failed to query fighter data. {0}"
            print(failure.format(e), file=sys.stderr)
            abort(404)

        current = Combatant(win[0], name[0][0], win[1])
        current.rank = rank
        current.wins = win[1]
        combatants.append(current)
        rank += 1

    return render_template('results.html', combatants=combatants)


if __name__ == '__main__':
    app.run(port=8047)
    cur.close()
